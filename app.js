/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'asgn1.Application',

    name: 'asgn1',

    requires: [
        // This will automatically load all classes in the asgn1 namespace
        // so that application classes do not need to require each other.
        'asgn1.*'
    ],

    // The name of the initial view to create.
    mainView: 'asgn1.view.main.Main'
});

Ext.define('asgn1.model.User', {
    extend: 'asgn1.model.Base',

    fields: [
        'name', 'class', 'section'
    ]
});

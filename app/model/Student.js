Ext.define('asgn1.model.Student', {
    extend: 'asgn1.model.Base',
    alias: 'model.Student',
    fields: [
        'fname', 'lname'
    ]
});

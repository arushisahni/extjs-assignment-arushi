Ext.define('asgn1.model.Personnel', {
    extend: 'asgn1.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});

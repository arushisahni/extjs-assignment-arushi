Ext.define('asgn1.store.Student', {
    extend: 'Ext.data.Store',

    alias: 'store.student',

    model: 'asgn1.model.Student',

    data: { items: [
        { fname: 'rushu', lname: 'tushu' },
       
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});


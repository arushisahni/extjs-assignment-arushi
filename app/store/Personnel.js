Ext.define('asgn1.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

   model: 'asgn1.model.Personnel',

    // data: { items: [
    //     { name: 'Jean Luc', email: "abcd@enterprise.com", phone: "555-111-1111" },
    //     { name: 'Worf',     email: "worf.moghsson@enterprise.com",  phone: "555-222-2222" },
    //     { name: 'Deanna',   email: "deanna.troi@enterprise.com",    phone: "555-333-3333" },
    //     { name: 'Data',     email: "mr.data@enterprise.com",        phone: "555-444-4444" }
    // ]},

    proxy: {
        type: 'ajax',
        url: 'https://jsonplaceholder.typicode.com/users',
       
},

autoLoad: true

});


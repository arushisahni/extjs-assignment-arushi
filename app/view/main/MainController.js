/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */

Ext.define('asgn1.view.main.MainController',{
    extend: 'Ext.app.ViewController',
    requires: ['asgn1.view.main.StudentViewModel'],
    viewModel: 'StudentViewModel',

    alias: 'controller.main',
    onSelected: function(sender,record)
    {
        console.log(record);
       
    },
     onItemSelected: function (sender,record)
     {

        // var store = this.getView().down('#childlist').getStore(),
        //     section = record[0].data.rno;
        
        console.log(record);
        var value=record[0].data.rno;
        console.log(value);
        //var store = this.getView().down('#childlist').getStore();
        var store = this.getView().down('extra').getViewModel().getStore('User');
        console.log(store);
        //store.filter('section',value);

        // var store = this.getViewModel().data;
        // console.log(store);
        store.filter('section',value);

        //var store = this.getViewModel().getStore('User');
             //section = record[0].data.rno;
        

        //store.filter('section',section);
    //    var store = this.getViewModel().getStore('store.user');
// // console.log(store);
// var users = user.findRecord('section', 'A');
// console.log(users.name);

//         //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        // Ext.Msg.alert('Confirm');
     },

    //  onButtonClick: function(button,b,c,d){
    //     var textField = button._text,
    //     view = button.getView();

    //     if(textField == 'Clear'){

    //     }
    //     else if(textField == 'Fill'){

    //     }
    //  },
     onClearClick: function(sender,record){
         //this.getView().down('form-view').reset();
         this.getView().down('formpanel').reset();
        //button.up('form').reset();
     },
     onSubmitClick: function(sender,record)
     {
        var data=this.getView().down('formpanel').getValues();
        console.log(data);
        var store = this.getView().down('extra').getViewModel().getStore('User');
        store.add({
            name: data.fname,
            class: data.class,
            section: data.section

        });
        Ext.Msg.alert('Data submitted successfully...');
        this.getView().down('formpanel').reset();
        //console.msg("Data submitted successfully");
     },
     onReadClick: function(sender,record)
     {

        // Ext.Ajax.request({
        //     url: 'https://jsonplaceholder.typicode.com/users',
        //     method: 'GET',
        //     timeout: 60000,
        //     params:
        //     {
        //         id: 1 // loads student whose Id is 1
        //     },
        //     headers:
        //     {
        //         'Content-Type': 'application/json'
        //     },
        //     success: function (response) {
                
        //     },
        //     failure: function (response) {
        //         Ext.Msg.alert('Status', 'Request Failed.');

        //     }
        // });

        var store = this.getView().down('extra').getViewModel().getStore('student'),
            data  =  store.data.items[0].data;
       
       this.getView().down('formpanel').setValues(data);


        //this.getView().down('form-view').g();
        //var form1 = button.up('form-view');
         //var form1= this.getView().down('form-view').getValues();
    //form1.loadRecord(new Student({fname:'aru',lname:'sahni'}));
    //this.getView().down('form-view');
    //  }
    //  var form1 = Ext.create('newForm');
        //  Ext.util.Observable.capture(form1)



    },
// callOnClick: function(){
//     alert("hello from controller");
// }

//     onConfirm: function (choice) {
//         if (choice === 'yes') {
//             //
//         }
//     }
// });


});

// 
// });Ext.define('Student', {
//     extend:'Ext.data.Model',
//     fields:[
//         'name'
//     ]
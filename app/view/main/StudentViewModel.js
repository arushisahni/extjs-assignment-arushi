Ext.define('asgn1.view.main.StudentViewModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.StudentViewModel',

    stores:{
        Teacher: {
            model: 'Teacher',
            autoload: true,
            data: { items: [
                { Tname: 'Vandana', Tclass: "9th", rno: "A" },
                { Tname: 'Rekha',     Tclass: "9th",  rno: "B" },
                { Tname: 'Shivani',   Tclass: "9th",    rno: "C" },
                { Tname: 'Pradeep',     Tclass: "9th",        rno: "D" }
            ]},
        
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    rootProperty: 'items'
                }
        }
    },
    User: {
        model: 'User',
        autoload: true,
        data: { items: [
            { name: 'anjali', class: "9th", section: "A" },
            { name: 'aarti',     class: "9th",  section: "B" },
            { name: 'arushi',   class: "9th",    section: "C" },
            { name: 'Data',     class: "9th",        section: "D" }
        ]},
        proxy: {
            type: 'memory',
            reader: {
                type: 'json',
                rootProperty: 'items'
            }
    }
},
student: {
    //model: 'Teacher',
    autoload: true,
    data: { items: [
        { fname: 'Arushi', lname: 'Sahni' , dob: '04/02/2018', fatherName: 'Rajiv', address: 'Delhi', city: 'New Delhi', state: 'Delhi', class: '9th', section: 'C'
    },
        
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
}
},
    }
    

});


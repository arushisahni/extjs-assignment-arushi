/**
 * This view is an example list of people.
 */
Ext.define('asgn1.view.main.Extra', {
    extend: 'Ext.panel.Panel',
    xtype: 'extra',
    
    requires: ['asgn1.view.main.StudentViewModel'],
    viewModel: 'StudentViewModel',
    // itemId:'extra',
    
    // controller: 'extraController',
    // itemId:'teacherlist',
    layout: 'vbox',
    
    // listeners: {
    //    select: 'onItemSelected'
    //  },
//     store:[
//         { name1: 'anjali', class: "9th", rno: "1" },
//    { name1: 'aarti',     class: "9th",  rno: "2" },
//      { name1: 'arushi',   class: "9th",    rno: "3" },
//      { name1: 'Data',     class: "9th",        rno: "4" }
//   ],
// store:{type:'teacher'},


// ],
// columns:[{
//     text:'Section',
//     dataIndex:'rno'
// }],
// plugins:'gridfilters',
// height: 300,
// width: 400,

// layout : {
//     type :'fit',
//     align: 'stretch'	
//  },
 
items:[
    {
        xtype:'grid',
        title: 'Parent Grid',
        height: '50%',
        listeners:[{
            select: 'onItemSelected'
        }],
        bind: {
            store: '{Teacher}'
        },
        // store: {
        //     type: 'teacher'
        // },
        columns: [{ 
        //     text: 'TeacherName',
        //     dataIndex: 'Tname',
        //     width: 200,
          
            
        // }, {
        //     text: 'TeacherClass',
        //     dataIndex: 'Tclass',
        //     width: 230 
        // }, 
         
            text: 'Section',
            dataIndex: 'rno',
            width: 150,
         
        }]

    },    {
       xtype:'grid',
       itemId:'childlist',
          title:'Child Grid',
          height: '50%',
          
          
    //       store:[{ name1: 'A', class: "9th", rno: "1" },
    //      { name1: 'B',     class: "9th",  rno: "2" },
    //      { name1: 'C',   class: "9th",    rno: "3" },
    //      { name1: 'D',     class: "9th",        rno: "4" }
    //  ],
    
    // store:{type:'user'},  
    bind:{
        store:'{User}'
    },
    
         columns: [{ 
             text: 'StudentName',
             dataIndex: 'name',
             width: 200,
             
             
             // cell: {
             //     userCls: 'bold'
             // }
         }, {
             text: 'StudentClass',
             dataIndex: 'class',
             width: 230 
     }, { 
            text: 'Section',
            dataIndex: 'section',
            width: 150 
          
        },
        
    ],
    
    }
]

});

//     // requires: [
//     //      'asgn1.store.User',
//     //      'asgn1.store.Personnel'
        

//     //  ],

//     title: 'Users',

//     //  store: {
//     //      type: 'user'
//     //  },
   
//  items:[{
//      xtype:'grid',
//      title:'Hello',
// //      store:[{ name1: 'anjali', class: "9th", rno: "1" },
// //     { name1: 'aarti',     class: "9th",  rno: "2" },
// //     { name1: 'arushi',   class: "9th",    rno: "3" },
// //     { name1: 'Data',     class: "9th",        rno: "4" }
// //  ],

    

//     columns: [{ 
//         text: 'Student',
//         dataIndex: 'name1',
//         width: 100,
//         // cell: {
//         //     userCls: 'bold'
//         // }
//     }, {
//         text: 'Class',
//         dataIndex: 'class',
//         width: 230 
//     }, { 
//         text: 'Rno',
//         dataIndex: 'rno',
//         width: 150 
//     },
    
// ],


// },{
//     xtype:'grid',
//     title:'HelloUser',
//     height:'50%',
//     columns: [{ 
//         text: 'StudentName',
//         dataIndex: 'name1',
//         width: 100,
//         // cell: {
//         //     userCls: 'bold'
//         // }
//     }, {
//         text: 'StudentClass',
//         dataIndex: 'class',
//         width: 230 
//     }, { 
//         text: 'StudentRno',
//         dataIndex: 'rno',
//         width: 150 
//     },
    
// ],
    

// }
// ]
    

    
//     /*listeners: {
//         select: 'onItemSelected'
//     }*/
// },

// );

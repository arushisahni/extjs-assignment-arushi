/**
 * This view is an example list of people.
 */
Ext.define('asgn1.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'asgn1.store.Personnel'
        

    ],

    title: 'Personnel',

    store: {
        type: 'personnel'
    },

    columns: [{ 
        text: 'Name',
        dataIndex: 'name',
        width: 100,
        cell: {
            userCls: 'bold'
        }
        
    },
    {
        text: 'Username',
        dataIndex: 'username',
        width: 230 
    },


     {
        text: 'Email',
        dataIndex: 'email',
        width: 230 
    }, { 
        text: 'Phone',
        dataIndex: 'phone',
        width: 150 
    },
    { 
        text: 'Website',
        dataIndex: 'website',
        width: 150 
    },
    { 
        text: 'Phone',
        dataIndex: 'phone',
        width: 150 
    },
   
],
    
   
    listeners: {
        select: 'onSelected'
    }
});


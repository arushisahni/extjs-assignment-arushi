Ext.define('asgn1.view.main.FormView', {
    extend: 'Ext.form.Panel',
    title: 'Student Form',
    
    xtype: 'form-view',
    //html: '<img src="student.jpg" height="100" width="100">',
    id: 'newForm',
    fullscreen: true,
    border:true,
    bodyPadding: 10,
   
    layout:{
      type: 'vbox',
      align: 'center'
  },

   store:{type:'Student'},
   items: [
      {
          xtype: 'fieldset',
          
          html: '<h2>Student Registation Form</h2>',
          
          border:true,
          instructions: '*Please Fill',
          
          layout:{
            type: 'vbox',
            align: 'center'
        },
        flex:1,
          
    items:[
       {
         
          
          xtype: 'formpanel',
          reference: 'form',
          autoSize: true,
          border:true,
          items: [
         //     {
         //    xtype:'image',
         //    id: 'south_image',
         //   html: <img src='/Desktop/student.jpg'/>,
         //    region: 'south',
         //    width: 700,
         //    height: 200
         //     },
             {
      
            xtype: 'textfield',
            label: 'First Name',
            name: 'fname'
            
         },{
            xtype: 'textfield',
            label: 'Last Name',
            name:'lname'
         },
         {
      
            xtype: 'datefield',
            label: 'DOB',
            name: 'dob'
            
         },
         {
      
            xtype: 'textfield',
            label: 'Class',
            name: 'class'
            
         },
         {
      
            xtype: 'textfield',
            label: 'Section',
            name: 'section'
            
         },
         {
      
            xtype: 'textfield',
            label: 'City',
            name: 'city'
            
         },
         {
      
            xtype: 'textfield',
            label: 'State',
            name: 'state'
            
         },
         {
                  xtype: 'button',
                  text: 'Clear',
                  
                  
                  handler: 'onClearClick'
                 
               },
               {
                  xtype: 'button',
                  text: 'Submit',
                  
                  handler: 'onSubmitClick'
                 
               },
               {
                  xtype: 'button',
                  text: 'Fill',
                  
                  handler: 'onReadClick'
                 
               },
              
               
      ]
       }
    ]
   }
   ]
   });
    
//     items: [{
      
//         xtype: 'textfield',
//         label: 'First Name',
//         name: 'fname'
        
//      },{
//         xtype: 'textfield',
//         label: 'Last Name',
//         name:'lname'
//      },
//    //   {
//    //      xtype: 'datefield',
//    //      label: 'Dob',
        
//    //   },
//      {
//         xtype: 'button',
//         text: 'Fill',
//         handler: 'onReadClick'
//      },
//      {
//       xtype: 'button',
//       text: 'Clear',
//       handler: 'onClearClick'
     
//    },
//    {
//       xtype: 'button',
//       text: 'Submit',
//       handler: function()
//       {
//          //var form=this.up('form-view');
//          var data = this.up('form-view');
//          //Ext.getCmp('newForm').getForm().submit();
//          //var data=this.up('form-view');
//          console.warn("print",data.getForm().getValues());
//       }
     
//    },
// ]
// }
// );
//    {
//         xtype: 'fieldcontainer',
//         label: 'Radio field',
//         defaultType: 'radiofield',
//         defaults: {
//            flex: 1
//         },
//         layout: 'hbox',
//         items: [{
//            boxLabel: 'A',
//            inputValue: 'a',
//            id: 'radio1'
//         },{
//            boxLabel: 'B',
//            inputValue: 'b',
//            id: 'radio2'
//         },{
//            boxLabel: 'C',
//            inputValue: 'c',
//            id: 'radio3'
//         }]
//      },{
//         xtype: 'fieldcontainer',
//         label: 'Check Box Field',
//         defaultType: 'checkboxfield',
        
//         items: [{
//            boxLabel: 'HTML',
//            inputValue: 'html',
//            id: 'checkbox1'
//         },{
//            boxLabel: 'CSS',
//            inputValue: 'css',
//            checked: true,
//            id: 'checkbox2'
//         },{
//            boxLabel: 'JavaScript',
//            inputValue: 'js',
//            id: 'checkbox3'
//         }]
//      },{
//         xtype: 'hiddenfield',
//         label: 'hidden field ',
//         value: 'value from hidden field'
//      },{
//         xtype: 'numberfield',
//         anchor: '100%',
//         label: 'Numeric Field',
//         maxValue: 99,
//         minValue: 0
//      },{
//         xtype: 'spinnerfield',
//         label: 'Spinner Field',
//         step: 6,
        
        
//      },{
//         xtype: 'timefield',
//         label: 'Time field',
//         minValue: '6:00 AM',
//         maxValue: '8:00 PM',
//         increment: 30,
//         anchor: '100%'
//      },{
//         xtype: 'combobox',
//         label: 'Combo Box',
//         store: Ext.create('Ext.data.Store', {
//            fields: ['abbr', 'name'],
//            data: [{
//               'abbr': 'HTML',
//               'name': 'HTML'
//            },{
//               'abbr': 'CSS',
//               'name': 'CSS'
//            },{
//               'abbr': 'JS',
//               'name': 'JavaScript'
//            }]
//         }),
//         valueField: 'abbr',
//         displayField: 'name'
//      },{
//         xtype: 'filefield',
//         label: 'File field',
//         labelWidth: 50,
//         msgTarget: 'side',
//         allowBlank: false,
//         anchor: '100%',
//         buttonText: 'Select File...'
//      }],
//    // buttons:[
//    //    {
//    //       text:'Fill',
//    //       handler:function(btn){
//    //          var data=this.up('form');
//    //          console.warn("print the form data",data.getView('form-view').getForm().getValues());
//    //       },
//    //       {
//    //          text:'Clear',
//    //          handler:function(btn){
//    //             var data=this.up('form');
//    //             console.warn("print the form data",data.getView('form-view').getForm().getValues());
//    //          }
//    //    }
//    // ]  
//   });

